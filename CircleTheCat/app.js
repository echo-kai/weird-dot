const stage = new createjs.Stage("circleDot");
createjs.Ticker.setFPS(60);
createjs.Ticker.addEventListener("tick",stage)

const gameView = new createjs.Container();
gameView.x = 30;
gameView.y = 30;
stage.addChild(gameView);

const circleArr = [[], [], [], [], [], [], [], [], []];

const board = new Board();
board.setBoard(circleArr);
board.setGameView(gameView);
board.setStage(stage);
board.init();
stage.update();