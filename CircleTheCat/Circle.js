const CircleType = {
    BLANK: 0,
    HINDER: 1,
    CAT: 2
};

function Circle() {
    createjs.Shape.call(this);
    this.setCircleType = function(type) {
        this._circleType = type;
    }
    this.setColor = function (colorString) {
        this.graphics.beginFill(colorString);
        this.graphics.drawCircle(0,0,25);
        this.graphics.endFill();
    }
    this.setBoard = function(board) {
        this.board = board;
    }
    this.setIndexY = function(indexY) {
        this.indexY = indexY;
        this.y = indexY * 55;
    }
    this.setIndexX = function(indexX) {
        this.indexX = indexX;
        this.x = this.indexY%2? indexX * 55 + 25 : indexX * 55;
    }
}
Circle.prototype = new createjs.Shape();