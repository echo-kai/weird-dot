function BlankCircle() {
    this.setCircleType(CircleType.BLANK);
    this.setColor("#cccccc");
    this.setCat = function(cat) {
        this.cat = cat;
    }
    this.click = function() {
        let indexX = this.indexX;
        let indexY = this.indexY;
        let newHinder = new HinderCircle();
        this.board.board[indexY][indexX] = newHinder;
        newHinder.setIndexY(indexY);
        newHinder.setIndexX(indexX);
        this.board.gameView.addChild(newHinder);
        this.cat.move();
    }
}
BlankCircle.prototype = new Circle();