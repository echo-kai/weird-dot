function CatCircle() {
    this.setCircleType(CircleType.CAT);
    this.setColor("#0000ff");
    let left, right, leftUp, rightUp, leftDown, rightDown;
    this.isOver = function() {
        let indexX = this.indexX;
        let indexY = this.indexY;
        if (indexX===0 || indexX===ROWNUMS-1 || indexY===0 || indexY===LINENUMS-1) return true;
        left  = this.board.board[indexY][indexX - 1];
        right = this.board.board[indexY][indexX + 1];
        leftUp = indexY%2===0 ? this.board.board[indexY-1][indexX-1]:this.board.board[indexY-1][indexX];
        rightUp = indexY%2===0 ? this.board.board[indexY-1][indexX]:this.board.board[indexY-1][indexX+1];
        leftDown = indexY%2===0 ? this.board.board[indexY+1][indexX-1]:this.board.board[indexY+1][indexY];
        rightDown = indexY%2===0 ? this.board.board[indexY+1][indexX]:this.board.board[indexY+1][indexX+1];
        return left._circleType === CircleType.HINDER && right._circleType === CircleType.HINDER && leftUp._circleType === CircleType.HINDER
            && rightUp._circleType === CircleType.HINDER && leftDown._circleType === CircleType.HINDER && rightDown._circleType === CircleType.HINDER;

    };
    this.move = function() {
        if (this.isOver()) {
            alert("游戏结束");
            return;
        }
        if (left._circleType===CircleType.BLANK) this.swap(left);
        else if (right._circleType===CircleType.BLANK) this.swap(right);
        else if (leftUp._circleType===CircleType.BLANK) this.swap(leftUp);
        else if (rightUp._circleType===CircleType.BLANK) this.swap(rightUp);
        else if (leftDown._circleType===CircleType.BLANK) this.swap(leftDown);
        else if (rightDown._circleType===CircleType.BLANK) this.swap(rightDown);
    }
    this.swap = function(blank) {
        [this.indexX,blank.indexX] = [blank.indexX,this.indexX];
        [this.indexY,blank.indexY] = [blank.indexY,this.indexY];
        this.setIndexY(this.indexY);
        this.setIndexX(this.indexX);
        blank.setIndexY(blank.indexY);
        blank.setIndexX(blank.indexX);
        [this.board.board[this.indexY][this.indexX],blank.board.board[blank.indexY][blank.indexX]] = [blank.board.board[blank.indexY][blank.indexX],this.board.board[this.indexY][this.indexX]];
    }
}
CatCircle.prototype = new Circle();
