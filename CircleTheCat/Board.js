const ROWNUMS = 9;
const LINENUMS = 9;
function Board() {
    this.setBoard = function(board) {
        this.board = board;
    }
    this.setGameView = function(gamaView) {
        this.gameView = gamaView;
    }
    this.setStage = function(stage) {
        this.stage = stage;
    }
    this.init = function() {
        for(let indexY = 0; indexY < LINENUMS; indexY++) {
            for(let indexX = 0; indexX < ROWNUMS; indexX++) {
                const circle = indexY===4 && indexX===4 ? new CatCircle():new BlankCircle();
                this.gameView.addChild(circle);
                this.board[indexY][indexX] = circle;
                circle.setIndexY(indexY);
                circle.setIndexX(indexX);
                circle.setBoard(this);
            }
        }
        for(let indexY = 0; indexY < LINENUMS; indexY++) {
            for (let indexX = 0; indexX < ROWNUMS; indexX++) {
                if (this.board[indexY][indexX]._circleType===CircleType.BLANK) {
                    this.board[indexY][indexX].setCat(this.board[parseInt(ROWNUMS/2)][parseInt(LINENUMS/2)]);
                    this.board[indexY][indexX].addEventListener("click",function(event) {
                        event.target.click();
                    });
                }
            }
        }
    }
}